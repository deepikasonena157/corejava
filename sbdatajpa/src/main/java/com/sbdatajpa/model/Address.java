package com.sbdatajpa.model;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonBackReference;

@Entity
@Table(name="addressjpa")
public class Address {
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer addId;
	
	@Column(name="city",length=25)
	private String city;
	@Column(name="state",length=25)
	private String state;
	
	//bi direction
	@ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "employee_id")  // FK
	@JsonBackReference
    private Employee employee;

	public Address() {
		super();
	}

	public Address(String city, String state, Employee employee) {
		super();
		this.city = city;
		this.state = state;
		this.employee = employee;
	}

	public Integer getAddId() {
		return addId;
	}

	public void setAddId(Integer addId) {
		this.addId = addId;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public Employee getEmployee() {
		return employee;
	}

	public void setEmployee(Employee employee) {
		this.employee = employee;
	}
	
	
	

}
