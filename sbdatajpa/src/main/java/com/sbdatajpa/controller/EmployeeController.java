package com.sbdatajpa.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.sbdatajpa.model.Employee;
import com.sbdatajpa.service.EmployeeService;

@RestController
@RequestMapping(value="employee")
public class EmployeeController {
	
	@Autowired              //has a relationship == wired == DI
	EmployeeService employeeService;
	
	@GetMapping(value="all")
	public List<Employee> list()
	{
		return employeeService.listAll();
	}
	
	@GetMapping(value="read/{data}")
	public Employee read(@PathVariable("data") Integer id)
	{
		return employeeService.readByEmployeeId(id);
	}

	@PostMapping(value="save")
	public ResponseEntity<Employee> save(@RequestBody Employee employee)  //OOP
	{
		return new ResponseEntity<Employee>(employeeService.save(employee),HttpStatus.OK);
	}
	
	@PutMapping(value="update")
	public ResponseEntity<Employee> update(@RequestBody Employee employee)
	{
		return new ResponseEntity<Employee>(employeeService.save(employee),HttpStatus.OK);
	}
	
	@DeleteMapping(value="delete/{data}")
	public Integer delete(@PathVariable("data") Integer id)
	{
		return employeeService.delete(id);
	}
	
	@GetMapping(value="des/{sapid}")
	public ResponseEntity<List<String>> searchByDesignation(@PathVariable String sapid)
	{
		System.out.println("Entered Sap Id:"+sapid);
		List<String> result = employeeService.searchByDesignation(sapid);
		return new ResponseEntity<List<String>>(result,HttpStatus.OK);
		
	}
	
	@GetMapping(value="twoparam/{sapid}/{designation}")
	public ResponseEntity<Employee> searchBy2Params(@PathVariable Integer sapid,@PathVariable String designation)
	{
		System.out.println("Entered Sap Id" + sapid);
		System.out.println("Entered Designation" + designation);
		Optional<Employee> result = employeeService.searchByIdAndDesignation(sapid,designation);
		if (result.isPresent())
		{
			return new ResponseEntity<Employee>(result.get(),HttpStatus.OK);
		}
		else
		{
			return new ResponseEntity<Employee>(result.get(),HttpStatus.NOT_FOUND);
		}
	}
	
	@GetMapping(value="twonative/{sapid}/{designation}")
	public ResponseEntity<Employee> twoParams(@PathVariable Integer sapid,@PathVariable String designation)
	{		
			return new ResponseEntity<Employee>(employeeService.nativeQuery(sapid,designation),HttpStatus.OK);
	}
	@GetMapping(value="break/{sapid}/{designation}")
	public ResponseEntity<Employee> twoParam(@PathVariable Integer sapid,@PathVariable String designation)
	{		
			return new ResponseEntity<Employee>(employeeService.nativeQuery(sapid,designation),HttpStatus.OK);
	}
	
}
