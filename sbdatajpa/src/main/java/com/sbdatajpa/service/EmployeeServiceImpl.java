package com.sbdatajpa.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.sbdatajpa.dao.EmployeeRepository;
import com.sbdatajpa.model.Employee;

@Service
public class EmployeeServiceImpl implements EmployeeService {

	@Autowired
	private EmployeeRepository employeeRepository;   //wire == DI
	
	@Transactional
	public List<Employee> listAll() {
		
		return employeeRepository.findAll();
	}

	@Transactional
	public Employee readByEmployeeId(Integer abc) {
		
		Employee employee = null;
		Optional<Employee> optional = employeeRepository.findById(abc);
		if(optional.isPresent())
		{
			employee=optional.get();
		}
		else
		{
			System.err.println("No such employee");  //raise an exception
		}
		return employee;
	}


	@Transactional
	public Employee save(Employee employee) {    //ORM
		
		return employeeRepository.save(employee);
	}

	@Transactional
	public Integer delete(Integer efg) {

		employeeRepository.deleteById(efg);
		return efg;
	}

	@Transactional
	public List<String> searchByDesignation(String hij) {
		
		return employeeRepository.getByDesignation(hij);
	}

	@Transactional
	public Optional<Employee> searchByIdAndDesignation(Integer lmn, String hij) {
	
		return employeeRepository.findByEmpIdAndDesignation(lmn, hij);
	}

	@Transactional
	public Employee nativeQuery(Integer lmn, String hij) {
		
		return employeeRepository.findByNativeQuery(lmn,hij);
	}

	@Transactional
	public Employee builtIn(Integer tea, String coffee) {
		
		return employeeRepository.findByEmpIdAndEmpName(tea, coffee);
	}

}
