package com.sbdatajpa.service;

import java.util.List;
import java.util.Optional;

import com.sbdatajpa.model.Employee;

public interface EmployeeService {
	
	public abstract List<Employee> listAll();    //read
	
	public abstract	Employee readByEmployeeId(Integer abc);
	
	public abstract	Employee save(Employee employee);   //create  and update
		
	public abstract	Integer delete(Integer efg);    //delete

	public abstract List<String> searchByDesignation(String hij);
	
	public abstract Optional<Employee> searchByIdAndDesignation(Integer lmn, String hij);
	
	public abstract Employee nativeQuery(Integer lmn, String hij);
	
	public abstract Employee builtIn(Integer tea, String coffee);
}
