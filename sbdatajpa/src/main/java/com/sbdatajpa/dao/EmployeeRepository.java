package com.sbdatajpa.dao;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.sbdatajpa.model.Employee;

//reduced the boiler code
public interface EmployeeRepository extends JpaRepository<Employee, Integer>{


    // findByProperty(Integer property)
    // findByProperty1OrProperty2(String property, String property2);
   
    // countByProperty(String property); ==> integer == long
    // List<Pojo> findDistinctByProperty(String property)
   
    // List<Pojo> findFirst3ByPropertyOrderByPropertyAsc(String property);
   

	Employee findByEmpIdAndEmpName(Integer empId, String empName);
	
	@Query("SELECT emp.empName FROM Employee as emp where emp.designation = :id")  // ORM == Property
	List<String> getByDesignation(@Param("id") String id);

	@Query("SELECT emp FROM Employee as emp where emp.empId = ?1 AND emp.designation = ?2")
	public Optional<Employee> findByEmpIdAndDesignation(Integer empId, String designation);

	@Query(value = "select * from employeejpa where emp_id = :?abc and emp_design =?efg",nativeQuery=true)
	public Employee findByNativeQuery(@Param("abc") Integer abc,@Param("efg") String efg);
}
