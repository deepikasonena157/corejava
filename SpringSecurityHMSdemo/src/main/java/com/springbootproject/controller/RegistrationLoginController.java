package com.springbootproject.controller;

import org.springframework.stereotype.Controller;



import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import com.springbootproject.model.RegistrationLogin;
import com.springbootproject.service.RegistrationService;



@Controller
@RequestMapping("/registration")
public class RegistrationLoginController {

	private RegistrationService registrationService;

	public RegistrationLoginController(RegistrationService registrationService) {
		super();
		this.registrationService = registrationService;
	}
	
	@ModelAttribute("user")
    public RegistrationLogin userRegistration() {
        return new RegistrationLogin();
    }
	
	@GetMapping
	public String showRegistrationForm() {
		return "registration";
	}
	
	@PostMapping
	public String registerUserAccount(@ModelAttribute("user") RegistrationLogin registrationLogin) {
		registrationService.save(registrationLogin);
		return "redirect:/registration?success";
	}
}
