package com.springbootproject.service;

import java.util.Arrays;

import java.util.Collection;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import com.springbootproject.model.Login;
import com.springbootproject.model.Registration;
import com.springbootproject.model.RegistrationLogin;
import com.springbootproject.repository.RegistrationRepository;



@Service
public class RegistrationServiceImpl implements RegistrationService{

	private RegistrationRepository registrationRepository;
	
	@Autowired
	private BCryptPasswordEncoder passwordEncoder;
	
	public RegistrationServiceImpl(RegistrationRepository registrationRepository) {
		super();
		this.registrationRepository = registrationRepository;
	}

	@Override
	public Registration save(RegistrationLogin registrationLogin) {
		Registration registration = new Registration(registrationLogin.getFirstName(), 
				registrationLogin.getLastName(), registrationLogin.getEmail(),
				passwordEncoder.encode(registrationLogin.getPassword()), Arrays.asList(new Login("login_user")));
		
		return registrationRepository.save(registration);
	}

	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
	
		Registration registration = registrationRepository.findByEmail(username);
		if(registration == null) {
			throw new UsernameNotFoundException("Invalid username or password.");
		}
		return new org.springframework.security.core.userdetails.User(registration.getEmail(), registration.getPassword(), mapRolesToAuthorities(registration.getRoles()));		
	}
	
	private Collection<? extends GrantedAuthority> mapRolesToAuthorities(Collection<Login> roles){
		return roles.stream().map(role -> new SimpleGrantedAuthority(role.getName())).collect(Collectors.toList());
	}
	
}
