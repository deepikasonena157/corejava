package com.springbootproject.service;

import org.springframework.security.core.userdetails.UserDetailsService;


import com.springbootproject.model.Registration;
import com.springbootproject.model.RegistrationLogin;



public interface RegistrationService extends UserDetailsService{
	Registration save(RegistrationLogin registrationLogin);
}
