package com.springbootproject.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.springbootproject.model.Registration;

@Repository
public interface RegistrationRepository extends JpaRepository<Registration, Long>{
	Registration findByEmail(String email);
}
